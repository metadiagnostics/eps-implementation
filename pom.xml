
<!-- Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com). 
	Licensed under the Apache License, Version 2.0 (the "License"); you may not 
	use this file except in compliance with the License. You may obtain a copy 
	of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required 
	by applicable law or agreed to in writing, software distributed under the 
	License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS 
	OF ANY KIND, either express or implied. See the License for the specific 
	language governing permissions and limitations under the License. -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>org.socraticgrid.hl7</groupId>
	<artifactId>eps-implementation-parent</artifactId>
	<version>0.1.5-SNAPSHOT</version>

	<name>EPS :: Implementation :: Parent</name>
	<description>The component is a trail implementation of the HL7 Event Publication and Subscription Service</description>
	<url>https://bitbucket.org/cogmedsys/eps-api</url>
	<licenses>
		<license>
			<name>The Apache License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
		</license>
	</licenses>
	<developers>
		<developer>
			<name>Jerry Goodnough</name>
			<email>jgoodnough@cognitivemedicine.com</email>
			<organization>Cognitive Medical Systems</organization>
			<organizationUrl>http://www.cognitivemedicine.com</organizationUrl>
		</developer>
	</developers>
	<scm>
		<connection>scm:git:bitbucket.org/cogmedsys/eps-api.git</connection>
		<developerConnection>scm:git:bitbucket.org/cogmedsys/eps-api.git</developerConnection>
		<url>https://bitbucket.org/cogmedsys/eps-api.git</url>
	</scm>
	<distributionManagement>
		<snapshotRepository>
			<id>ossrh</id>
			<url>https://oss.sonatype.org/content/repositories/snapshots</url>
		</snapshotRepository>
		<repository>
			<id>ossrh</id>
			<url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
		</repository>
	</distributionManagement>
	
	<packaging>pom</packaging>

	<modules>
		<module>eps-delivery-intvn-soap</module>
		<module>eps-delivery-intvn-spring</module>
		<module>eps-subscriber-client-ri</module>
		<module>eps-subscriber-library</module>
		<module>eps-subscriber-testservice</module>
		<module>eps-engine-ri</module>
		<module>eps-service-library</module>
		<module>eps-publisher-intvn-soap</module>
		<module>eps-publisher-intvn-spring</module>
		<module>eps-publisher-client-ri</module>
		<module>eps-publisher-library</module>
		<module>eps-webservice</module>
	</modules>

	<properties>
		<java.version>1.7</java.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

		<!-- EPS -->
		<eps.version>0.1.5-SNAPSHOT</eps.version>

		<!-- Spring Configurator -->
		<spring.configurator.version>0.0.1-SNAPSHOT</spring.configurator.version>


		<!-- Web -->
		<jsp.version>2.2</jsp.version>
		<jstl.version>1.2</jstl.version>
		<servlet.version>2.5</servlet.version>
		<jaxws.version>2.2.10</jaxws.version>
		<jaxws.api.version>2.2.11</jaxws.api.version>
		<javax.ws.rs.version>[2.0,3)</javax.ws.rs.version>

		<cxf.version>3.0.3</cxf.version>

		<!-- Spring -->
		<spring.version>3.2.3.RELEASE</spring.version>

		<!-- Jackson -->
		<jackson.version>2.5.4</jackson.version>

		<!-- Kafka -->
		<kafka.version>0.8.2.0</kafka.version>

		<!-- Scala -->
		<scala.version>2.9.2</scala.version>

		<!-- zkclient -->
		<zkclient.version>0.3</zkclient.version>

		<!-- Google -->
		<google.guava.version>14.0.1</google.guava.version>


		<!-- Logging -->
		<logback.version>1.0.13</logback.version>
		<slf4j.version>1.7.5</slf4j.version>

		<!-- Test -->
		<junit.version>4.11</junit.version>

		<!-- Misc -->
		<netbeans.hint.license>apache20</netbeans.hint.license>
	</properties>

	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<configuration>
						<source>${java.version}</source>
						<target>${java.version}</target>
					</configuration>
				</plugin>
				<!-- Sonotype requirements follow -->
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-javadoc-plugin</artifactId>
					<version>2.10.1</version>
					<executions>
						<execution>
							<id>attach-javadocs</id>
							<goals>
								<goal>jar</goal>
							</goals>
						</execution>
					</executions>
					<configuration>
						<show>private</show>
						<nohelp>true</nohelp>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-source-plugin</artifactId>
					<version>2.4</version>
					<executions>
						<execution>
							<id>attach-sources</id>
							<phase>verify</phase>
							<goals>
								<goal>jar-no-fork</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
				<!-- Artifact signing -->
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-gpg-plugin</artifactId>
					<version>1.5</version>
					<executions>
						<execution>
							<id>sign-artifacts</id>
							<phase>verify</phase>
							<goals>
								<goal>sign</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
				<!-- Release Staging -->
				<plugin>
					<groupId>org.sonatype.plugins</groupId>
					<artifactId>nexus-staging-maven-plugin</artifactId>
					<version>1.6.3</version>
					<extensions>true</extensions>
					<configuration>
						<serverId>ossrh</serverId>
						<nexusUrl>https://oss.sonatype.org/</nexusUrl>
						<autoReleaseAfterClose>true</autoReleaseAfterClose>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<dependencyManagement>
		<dependencies>

			<!-- EPS API -->
			<dependency>
				<groupId>org.socraticgrid.hl7</groupId>
				<artifactId>eps-api</artifactId>
				<version>${eps.version}</version>
			</dependency>

			<!-- Internal Dependencies -->

			<dependency>
				<groupId>org.socraticgrid.hl7</groupId>
				<artifactId>eps-implementation-subscriber-library</artifactId>
				<version>${project.version}</version>
			</dependency>

			<dependency>
				<groupId>org.socraticgrid.hl7</groupId>
				<artifactId>eps-implementation-subscriber-client-ri</artifactId>
				<version>${project.version}</version>
			</dependency>
			<dependency>
				<groupId>org.socraticgrid.hl7</groupId>
				<artifactId>eps-implementation-engine-ri</artifactId>
				<version>${project.version}</version>
			</dependency>
			<dependency>
				<groupId>org.socraticgrid.hl7</groupId>
				<artifactId>eps-implementation-engine-ri</artifactId>
				<version>${project.version}</version>
				<classifier>PublicationService</classifier>
				<type>wsdl</type>
			</dependency>
			<dependency>
				<groupId>org.socraticgrid.hl7</groupId>
				<artifactId>eps-implementation-engine-ri</artifactId>
				<version>${project.version}</version>
				<classifier>BrokerService</classifier>
				<type>wsdl</type>
			</dependency>
			<dependency>
				<groupId>org.socraticgrid.hl7</groupId>
				<artifactId>eps-implementation-engine-ri</artifactId>
				<version>${project.version}</version>
				<classifier>BrokerManagementService</classifier>
				<type>wsdl</type>
			</dependency>
			<dependency>
				<groupId>org.socraticgrid.hl7</groupId>
				<artifactId>eps-implementation-engine-ri</artifactId>
				<version>${project.version}</version>
				<classifier>SubscriptionService</classifier>
				<type>wsdl</type>
			</dependency>
			<dependency>
				<groupId>org.socraticgrid.hl7</groupId>
				<artifactId>eps-implementation-engine-ri</artifactId>
				<version>${project.version}</version>
				<classifier>TopicManagementService</classifier>
				<type>wsdl</type>
			</dependency>
			<dependency>
				<groupId>org.socraticgrid.hl7</groupId>
				<artifactId>eps-implementation-subscriber-client-ri</artifactId>
				<version>${project.version}</version>
				<classifier>SubscriberService</classifier>
				<type>wsdl</type>
			</dependency>


			<!-- Spring Configurator -->
			<dependency>
				<groupId>org.socraticgrid.spring.tools</groupId>
				<artifactId>SpringConfigurator</artifactId>
				<version>${spring.configurator.version}</version>
			</dependency>

			<!-- Spring -->
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-core</artifactId>
				<version>${spring.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-beans</artifactId>
				<version>${spring.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-aop</artifactId>
				<version>${spring.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-context</artifactId>
				<version>${spring.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-web</artifactId>
				<version>${spring.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-webmvc</artifactId>
				<version>${spring.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-tx</artifactId>
				<version>${spring.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-test</artifactId>
				<version>${spring.version}</version>
				<scope>test</scope>
			</dependency>

			<!-- CXF -->
			<dependency>
				<groupId>org.apache.cxf</groupId>
				<artifactId>cxf-rt-frontend-jaxws</artifactId>
				<version>${cxf.version}</version>
			</dependency>
			<dependency>
				<groupId>org.apache.cxf</groupId>
				<artifactId>cxf-rt-transports-http</artifactId>
				<version>${cxf.version}</version>
			</dependency>
			<dependency>
				<groupId>org.apache.cxf</groupId>
				<artifactId>cxf-rt-frontend-jaxrs</artifactId>
				<version>${cxf.version}</version>
			</dependency>
			<dependency>
				<groupId>org.apache.cxf</groupId>
				<artifactId>cxf-rt-rs-service-description</artifactId>
				<version>${cxf.version}</version>
			</dependency>
			<!-- Make sure that 4.4.1 Woodstoc is used or CXF will fail -->
			<dependency>
				<groupId>org.codehaus.woodstox</groupId>
				<artifactId>woodstox-core-asl</artifactId>
				<version>4.4.1</version>
			</dependency>

			<!-- ========= JACKSON 2.0 ================= -->
			<!-- Core classes, which includes Streaming API, shared low-level abstractions 
				(but NOT data-binding) -->
			<dependency>
				<groupId>com.fasterxml.jackson.core</groupId>
				<artifactId>jackson-core</artifactId>
				<version>${jackson.version}</version>
			</dependency>
			<!-- Just the annotations; use this dependency if you want to attach annotations 
				to classes without connecting them to the code. -->
			<dependency>
				<groupId>com.fasterxml.jackson.core</groupId>
				<artifactId>jackson-annotations</artifactId>
				<version>${jackson.version}</version>
			</dependency>
			<!-- For databinding; ObjectMapper, JsonNode and related classes are here -->
			<dependency>
				<groupId>com.fasterxml.jackson.core</groupId>
				<artifactId>jackson-databind</artifactId>
				<version>${jackson.version}</version>
			</dependency>


			<!-- JAX-RS provider -->
			<dependency>
				<groupId>com.fasterxml.jackson.jaxrs</groupId>
				<artifactId>jackson-jaxrs-json-provider</artifactId>
				<version>${jackson.version}</version>
			</dependency>

			<!-- Kafka -->
			<dependency>
				<groupId>org.apache.kafka</groupId>
				<artifactId>kafka_2.9.2</artifactId>
				<version>${kafka.version}</version>
			</dependency>

			<!-- Google -->
			<dependency>
				<artifactId>guava</artifactId>
				<groupId>com.google.guava</groupId>
				<type>jar</type>
				<version>${google.guava.version}</version>
			</dependency>

			<!-- MYBATIS -->
			<dependency>
				<groupId>org.mybatis</groupId>
				<artifactId>mybatis</artifactId>
				<version>3.0.1</version>
			</dependency>

			<!-- JUnit -->
			<dependency>
				<groupId>junit</groupId>
				<artifactId>junit</artifactId>
				<version>${junit.version}</version>
				<scope>test</scope>
			</dependency>

			<!-- SLF4J - Log4J -->
			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>slf4j-log4j12</artifactId>
				<version>1.7.5</version>
			</dependency>
			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>slf4j-api</artifactId>
				<version>1.7.5</version>
			</dependency>


			<!-- Mockito -->
			<dependency>
				<groupId>org.mockito</groupId>
				<artifactId>mockito-all</artifactId>
				<version>1.9.5</version>
				<scope>test</scope>
			</dependency>

			<!-- Javax -->
			<dependency>
				<groupId>com.sun.xml.ws</groupId>
				<artifactId>jaxws-rt</artifactId>
				<version>${jaxws.version}</version>
			</dependency>
			<dependency>
				<groupId>javax.xml.ws</groupId>
				<artifactId>jaxws-api</artifactId>
				<version>${jaxws.api.version}</version>
			</dependency>
			<dependency>
				<groupId>org.jvnet.jax-ws-commons.spring</groupId>
				<artifactId>jaxws-spring</artifactId>
				<version>1.9</version>
			</dependency>
			<dependency>
				<groupId>org.jvnet.staxex</groupId>
				<artifactId>stax-ex</artifactId>
				<version>1.7.7</version>
			</dependency>

			<!-- Scala -->
			<dependency>
				<groupId>org.scala-lang</groupId>
				<artifactId>scala-library</artifactId>
				<version>${scala.version}</version>
			</dependency>

			<!-- ZKClient -->
			<dependency>
				<groupId>com.101tec</groupId>
				<artifactId>zkclient</artifactId>
				<version>${zkclient.version}</version>
			</dependency>

			<!-- DERBY -->
			<dependency>
				<groupId>org.apache.derby</groupId>
				<artifactId>derbyclient</artifactId>
				<version>10.10.1.1</version>
			</dependency>



			<!-- Zookeeper -->
			<dependency>
				<groupId>org.apache.zookeeper</groupId>
				<artifactId>zookeeper</artifactId>
				<version>3.4.6</version>
			</dependency>

		</dependencies>
	</dependencyManagement>
</project>
