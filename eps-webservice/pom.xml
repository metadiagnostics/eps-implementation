
<!-- Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com). 
	Licensed under the Apache License, Version 2.0 (the "License"); you may not 
	use this file except in compliance with the License. You may obtain a copy 
	of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required 
	by applicable law or agreed to in writing, software distributed under the 
	License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS 
	OF ANY KIND, either express or implied. See the License for the specific 
	language governing permissions and limitations under the License. -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.socraticgrid.hl7</groupId>
		<artifactId>eps-implementation-parent</artifactId>
		<version>0.1.5-SNAPSHOT</version>
	</parent>

	<artifactId>eps-implementation-webservice</artifactId>
	<packaging>war</packaging>

	<name>EPS :: Implementation :: Web Service Wrapper</name>
	<description>The component is a trail implementation of the HL7 Event Publication and Subscription Service</description>
	<url>https://bitbucket.org/cogmedsys/eps-implementation</url>
	<licenses>
		<license>
			<name>The Apache License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
		</license>
	</licenses>
	<developers>
		<developer>
			<name>Jerry Goodnough</name>
			<email>jgoodnough@cognitivemedicine.com</email>
			<organization>Cognitive Medical Systems</organization>
			<organizationUrl>http://www.cognitivemedicine.com</organizationUrl>
		</developer>
	</developers>
	<scm>
		<connection>scm:git:bitbucket.org/cogmedsys/eps-implementation.git</connection>
		<developerConnection>scm:git:bitbucket.org/cogmedsys/eps-implementation.git</developerConnection>
		<url>https://bitbucket.org/cogmedsys/eps-implementation.git</url>
	</scm>

	<distributionManagement>
		<snapshotRepository>
			<id>ossrh</id>
			<url>https://oss.sonatype.org/content/repositories/snapshots</url>
		</snapshotRepository>
		<repository>
			<id>ossrh</id>
			<url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
		</repository>
	</distributionManagement>
	<dependencies>

		<dependency>
			<groupId>org.socraticgrid.hl7</groupId>
			<artifactId>eps-api</artifactId>
		</dependency>

		<dependency>
			<groupId>org.socraticgrid.hl7</groupId>
			<artifactId>eps-implementation-engine-ri</artifactId>
		</dependency>

		<dependency>
			<groupId>com.sun.xml.ws</groupId>
			<artifactId>jaxws-rt</artifactId>
		</dependency>

		<dependency>
			<groupId>javax.xml.ws</groupId>
			<artifactId>jaxws-api</artifactId>
		</dependency>

		<!-- CXF -->
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-frontend-jaxws</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-transports-http</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-frontend-jaxrs</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-rs-service-description</artifactId>
		</dependency>
		<dependency>
			<groupId>org.jvnet.jax-ws-commons.spring</groupId>
			<artifactId>jaxws-spring</artifactId>
		</dependency>



		<!-- ========= JACKSON 2.0 ================= -->
		<!-- Core classes, which includes Streaming API, shared low-level abstractions 
			(but NOT data-binding) -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-core</artifactId>
		</dependency>
		<!-- Just the annotations; use this dependency if you want to attach annotations 
			to classes without connecting them to the code. -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-annotations</artifactId>
		</dependency>
		<!-- For databinding; ObjectMapper, JsonNode and related classes are here -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
		</dependency>
		<!-- JAX-RS provider -->
		<dependency>
			<groupId>com.fasterxml.jackson.jaxrs</groupId>
			<artifactId>jackson-jaxrs-json-provider</artifactId>
		</dependency>
		<!-- ========= JACKSON 2.0 ================= -->
	</dependencies>

	<build>
		<finalName>EPSWebService</finalName>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.1</version>
				<configuration>
					<source>${java.version}</source>
					<target>${java.version}</target>
				</configuration>
			</plugin>
			<!-- Provide Logging control in Unit Test & Exclude Integration tests -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.16</version>
				<configuration>
					<systemProperties>
						<property>
							<name>java.util.logging.config.file</name>
							<value>src/test/resources/logging.properties</value>
						</property>
					</systemProperties>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<version>2.4</version>
				<executions>
					<execution>
						<phase>compile</phase>
						<goals>
							<goal>exploded</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
</project>