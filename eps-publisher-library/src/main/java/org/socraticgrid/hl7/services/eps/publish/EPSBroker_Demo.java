package org.socraticgrid.hl7.services.eps.publish;

import org.socraticgrid.hl7.services.eps.exceptions.AuthenicationRequiredException;
import org.socraticgrid.hl7.services.eps.exceptions.InvalidDataException;
import org.socraticgrid.hl7.services.eps.exceptions.NoSuchTopicException;
import org.socraticgrid.hl7.services.eps.exceptions.NotAuthorizedException;
import org.socraticgrid.hl7.services.eps.model.Message;
import org.socraticgrid.hl7.services.eps.model.MessageBody;
import org.socraticgrid.hl7.services.eps.model.User;

public class EPSBroker_Demo {

	public static void main(String[] args) {
		EPSBroker broker  = new EPSBroker();
		String serviceAddress="http://localhost:8080/EPSWebService";
		String topic = "/Test";
		broker.setBrokerEndpoint(serviceAddress);
		User user = new User();
		user.setUserId("Test");
		user.setName("Test");
		broker.setPublisher(user);
		Message event = new Message();
		event.getHeader().setTopicId(topic);
		event.getHeader().setSubject("Test subject");
		MessageBody body = new MessageBody();
		body.setType("text");
		body.setBody("Sample text");
		event.getMessageBodies().add(body);
		try {
			String id = broker.publishMessageToTopic(topic, event);
			System.out.println("Message Id = "+id);
		} catch (NotAuthorizedException | AuthenicationRequiredException | NoSuchTopicException
				| InvalidDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

}
