/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.hl7.services.eps;

import java.util.LinkedList;
import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.socraticgrid.hl7.services.eps.exceptions.AuthenicationRequiredException;
import org.socraticgrid.hl7.services.eps.exceptions.ConflictException;
import org.socraticgrid.hl7.services.eps.exceptions.ExpiredException;
import org.socraticgrid.hl7.services.eps.exceptions.FeatureNotAvailableException;
import org.socraticgrid.hl7.services.eps.exceptions.IncompleteDataException;
import org.socraticgrid.hl7.services.eps.exceptions.InvalidDataException;
import org.socraticgrid.hl7.services.eps.exceptions.MediaFormatNotExceptedException;
import org.socraticgrid.hl7.services.eps.exceptions.NoSuchItemException;
import org.socraticgrid.hl7.services.eps.exceptions.NoSuchTopicException;
import org.socraticgrid.hl7.services.eps.exceptions.NotAuthorizedException;
import org.socraticgrid.hl7.services.eps.exceptions.PubSubException;
import org.socraticgrid.hl7.services.eps.interfaces.BrokerManagementIFace;
import org.socraticgrid.hl7.services.eps.model.BrokerAccessRequest;
import org.socraticgrid.hl7.services.eps.model.CreationResult;
import org.socraticgrid.hl7.services.eps.model.Options;
import org.socraticgrid.hl7.services.eps.model.PublicationContract;
import org.socraticgrid.hl7.services.eps.model.RemoveContentScope;
import org.socraticgrid.hl7.services.eps.model.Subscription;
import org.socraticgrid.hl7.services.eps.model.Topic;
import org.socraticgrid.hl7.services.eps.model.TopicMetadata;
import org.socraticgrid.hl7.services.eps.model.User;
import org.socraticgrid.hl7.services.eps.service.impl.BrokerManagementServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@Path("/brokermanagement")
@WebService(name = "brokermanagement", targetNamespace = "org.socraticgrid.hl7.services.eps")
public class BrokerManagementService extends ServiceBase implements BrokerManagementIFace
{

	@Autowired
	@Qualifier("BrokerManagementServiceImpl")
	BrokerManagementServiceImpl brokerManagementImpl;
	private final Logger logger = LoggerFactory
			.getLogger(BrokerManagementService.class);

	public BrokerManagementService()
	{

	}

	@Override
	@POST
	@Path("/createtopic")
    @Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public CreationResult createTopic(
			@WebParam(name = "parentTopic") @QueryParam("parenttopic") String parentTopic,
			@WebParam(name = "topicName") @QueryParam("topic") String topicName,
			@WebParam(name = "topicOptions") Topic topicOptions)
			throws NotAuthorizedException, AuthenicationRequiredException,
			ConflictException, NoSuchTopicException,
			MediaFormatNotExceptedException, ExpiredException,
			FeatureNotAvailableException, InvalidDataException,
			IncompleteDataException
	{

		CreationResult out;
		try
		{
			logger.debug("Calling createTopic");
			out = brokerManagementImpl.createTopic(getUser(), parentTopic, topicName,
					topicOptions);
		}
		catch (PubSubException exp)
		{
			logger.warn("Expected possible Exception createTopic", exp);
			throw exp;
		}
		catch (Throwable exp)
		{
			logger.error("Unexcepted Exception createTopic", exp);
			out = CreationResult.Failure;
		}

		return out;
	}

	@Override
	@POST
	@Path("/deletetopic")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public boolean deleteTopic(@WebParam(name = "topic") String topic)
			throws NotAuthorizedException, AuthenicationRequiredException,
			ConflictException, NoSuchTopicException
	{
		boolean out = false;
		try
		{
			logger.debug("Calling deleteTopic");
			out = brokerManagementImpl.deleteTopic(getUser(),topic);
		}
		catch (PubSubException exp)
		{
			logger.warn("Expected possible Exception deleteTopic", exp);
			throw exp;
		}
		catch (Throwable exp)
		{
			logger.error("Unexcepted Exception deleteTopic", exp);
			out = false;
		}

		return out;
	}

	@Override
	@POST
	@Path("/configuretopic")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public boolean configureTopic(@WebParam(name = "topic") @QueryParam("topic") String topic,
			@WebParam(name = "topicOptions") Topic topicOptions)
			throws NotAuthorizedException, AuthenicationRequiredException,
			ConflictException, NoSuchTopicException,
			MediaFormatNotExceptedException, ExpiredException,
			FeatureNotAvailableException, InvalidDataException,
			IncompleteDataException
	{
		boolean out = false;
		try
		{
			logger.debug("Calling configureTopic");
			out = brokerManagementImpl.configureTopic(getUser(),topic, topicOptions);
		}
		catch (PubSubException exp)
		{
			logger.warn("Expected possible Exception configureTopic", exp);
			throw exp;
		}
		catch (Throwable exp)
		{
			logger.error("Unexcepted Exception configureTopic", exp);
			out = false;
		}
		return out;
	}

	@Override
	@GET
	@Path("/gettopicmetadata")
    @Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public TopicMetadata getTopicMetadata(@WebParam(name = "topic") @QueryParam("topic") String topic)
			throws NotAuthorizedException, AuthenicationRequiredException,
			ExpiredException, NoSuchTopicException
	{
		TopicMetadata out;
		try
		{
			logger.debug("Calling getTopicMetadata");
			out = brokerManagementImpl.getTopicMetadata(getUser(),topic);
		}
		catch (PubSubException exp)
		{
			logger.warn("Expected possible Exception getTopicMetadata", exp);
			throw exp;
		}
		catch (Throwable exp)
		{
			logger.error("Unexcepted Exception getTopicMetadata", exp);
			out = null;
		}
		return out;
	}

	@Override
	@GET
	@Path("/getpublisherfortopic")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public List<PublicationContract> getPublishersForTopic(
			@WebParam(name = "topic") @QueryParam("topic") String topic)
			throws NotAuthorizedException, AuthenicationRequiredException,
			ExpiredException, NoSuchTopicException
	{
		List<PublicationContract> out;
		try
		{
			logger.debug("Calling getPublishersForTopic");
			out = brokerManagementImpl.getPublishersForTopic(getUser(),topic);
		}
		catch (PubSubException exp)
		{
			logger.warn("Expected possible Exception getPublishersForTopic", exp);
			throw exp;
		}
		catch (Throwable exp)
		{
			logger.error("Unexcepted Exception getPublishersForTopic", exp);
			out = new LinkedList<PublicationContract>();
		}
		return out;
	}

	@Override
	@GET
	@Path("/getsubscriptionsfortopic")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public List<Subscription> getSubscriptionsForTopic(
			@WebParam(name = "topic") @QueryParam("topic") String topic)
			throws NotAuthorizedException, AuthenicationRequiredException,
			ExpiredException, NoSuchTopicException
	{
		List<Subscription> out;
		try
		{
			logger.debug("Calling getSubscriptionsForTopic");
			out = brokerManagementImpl.getSubscriptionsForTopic(getUser(),topic);
		}
		catch (PubSubException exp)
		{
			logger.warn("Expected possible Exception getSubscriptionsForTopic", exp);
			throw exp;
		}
		catch (Throwable exp)
		{
			logger.error("Unexcepted Exception getSubscriptionsForTopic", exp);
			out = new LinkedList<Subscription>();
		}
		return out;
	}

	@Override
	@POST
	@Path("/purgealltopicevents")
    @Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public boolean purgeAllTopicEvents(@WebParam(name = "topic") @QueryParam("topic") String topic)
			throws NotAuthorizedException, AuthenicationRequiredException,
			NoSuchTopicException
	{
		boolean out = false;
		try
		{
			logger.debug("Calling purgeAllTopicEvents");
			out = brokerManagementImpl.purgeAllTopicEvents(getUser(),topic);
		}
		catch (PubSubException exp)
		{
			logger.warn("Expected possible Exception purgeAllTopicEvents", exp);
			throw exp;
		}
		catch (Throwable exp)
		{
			logger.error("Unexcepted Exception purgeAllTopicEvents", exp);
			out = false;
		}
		return out;
	}

	@Override
	@PUT
	@Path("/user")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public boolean createUser(@WebParam(name = "userInfo") User userInfo)
			throws NotAuthorizedException, AuthenicationRequiredException,
			ConflictException, InvalidDataException, IncompleteDataException
	{
		boolean out = false;
		try
		{
			logger.debug("Calling createUser");
			out = brokerManagementImpl.createUser(getUser(),userInfo);
		}
		catch (PubSubException exp)
		{
			logger.warn("Expected possible Exception createUser", exp);
			throw exp;
		}
		catch (Throwable exp)
		{
			logger.error("Unexcepted Exception createUser", exp);
			out = false;
		}
		return out;
	}

	@Override
	@DELETE
	@Path("/user/{userId}")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public boolean deleteUser(@WebParam(name = "userId") @PathParam("userId") String userId,
			@WebParam(name = "query") @QueryParam("scope") RemoveContentScope scope)
			throws NotAuthorizedException, AuthenicationRequiredException,
			NoSuchItemException
	{
		boolean out = false;
		try
		{
			logger.debug("Calling deleteUser");
			out = brokerManagementImpl.deleteUser(getUser(),userId, scope);
		}
		catch (PubSubException exp)
		{
			logger.warn("Expected possible Exception deleteUser", exp);
			throw exp;
		}
		catch (Throwable exp)
		{
			logger.error("Unexcepted Exception deleteUser", exp);
			out = false;
		}
		return out;
	}

	@Override
	@GET
	@Path("/user/{userId}")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public User getUser(@WebParam(name = "userId") @PathParam("userId") String userId)
			throws NotAuthorizedException, AuthenicationRequiredException,
			InvalidDataException, NoSuchItemException
	{

		User out;
		try
		{
			logger.debug("Calling getUser");
			out = brokerManagementImpl.getUser(getUser(),userId);
		}
		catch (PubSubException exp)
		{
			logger.warn("Expected possible Exception getUser", exp);
			throw exp;
		}
		catch (Throwable exp)
		{
			logger.error("Unexcepted Exception getUser", exp);
			out = null;
		}
		return out;
	}

	@Override
	@GET
	@Path("/user")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})	
	public List<User> discoverUser(@WebParam(name = "query") @QueryParam("query") String query)
			throws NotAuthorizedException, AuthenicationRequiredException,
			InvalidDataException, NoSuchItemException
	{
		List<User> out;
		try
		{
			logger.debug("Calling discoverUser");
			out = brokerManagementImpl.discoverUser(getUser(),query);
		}
		catch (PubSubException exp)
		{
			logger.warn("Expected possible Exception discoverUser", exp);
			throw exp;
		}
		catch (Throwable exp)
		{
			logger.error("Unexcepted Exception discoverUser", exp);
			out = new LinkedList<User>();
		}
		return out;
	}

	// TODO  Update Update Contract
	@Override
	@PUT
	@Path("/movetopic")
	public void moveTopic(@WebParam(name = "sourceTopic") @QueryParam("sourceTopic") String sourceTopic,
			@WebParam(name = "destinationPath") @QueryParam("destinationPath") String destinationPath)
			throws NotAuthorizedException, AuthenicationRequiredException,
			ConflictException, NoSuchTopicException
	{
		try
		{
			logger.debug("Calling moveTopic");
			brokerManagementImpl.moveTopic(getUser(),sourceTopic, destinationPath);
		}
		catch (PubSubException exp)
		{
			logger.warn("Expected possible Exception moveTopic", exp);
			throw exp;
		}
		catch (Throwable exp)
		{
			logger.error("Unexcepted Exception moveTopic", exp);
			//TODO  Throw the correct high level Exception - TBD
		}

	}
	
	// TODO  Update Update Contract
	@Override
	@PUT
	@Path("/user/{userId}")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})	
	public void updateUser(@WebParam(name = "userId") @PathParam("userId") String userId,
			@WebParam(name = "options") @QueryParam("options") Options options,
			@WebParam(name = "userData") User userData)
			throws NotAuthorizedException, AuthenicationRequiredException,
			NoSuchItemException, MediaFormatNotExceptedException,
			InvalidDataException, IncompleteDataException
	{
		try
		{
			logger.debug("Calling configureTopic");
			brokerManagementImpl.updateUser(getUser(),userId, options, userData);
		}
		catch (PubSubException exp)
		{
			logger.warn("Expected possible Exception configureTopic", exp);
			throw exp;
		}
		catch (Throwable exp)
		{
			logger.error("Unexcepted Exception configureTopic", exp);
			//TODO  Throw the correct high level Exception - TBD
		}

	}

	
	@GET
	@Path("/accessrequests")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public List<BrokerAccessRequest> getBrokerAccessRequests()
			throws NotAuthorizedException, AuthenicationRequiredException,
			FeatureNotAvailableException
	{

		List<BrokerAccessRequest> out;
		try
		{
			logger.debug("Calling getBrokerAccessRequests");
			out = brokerManagementImpl.getBrokerAccessRequests(getUser());
		}
		catch (PubSubException exp)
		{
			logger.warn("Expected possible Exception getBrokerAccessRequests", exp);
			throw exp;
		}
		catch (Throwable exp)
		{
			logger.error("Unexcepted Exception getBrokerAccessRequests", exp);
			out = new LinkedList<BrokerAccessRequest>();
		}
		return out;
	}

	@PUT
	@Path("/accessrequests/grant")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})	
	public boolean grantBrokerAccessRequest(
			@WebParam(name = "requests") List<BrokerAccessRequest> request)
			throws NotAuthorizedException, AuthenicationRequiredException,
			NoSuchItemException, InvalidDataException, IncompleteDataException,
			FeatureNotAvailableException
	{
		boolean out = false;
		try
		{
			logger.debug("Calling grantBrokerAccessRequest");
			out = brokerManagementImpl.grantBrokerAccessRequest(getUser(),request);
		}
		catch (PubSubException exp)
		{
			logger.warn("Expected possible Exception grantBrokerAccessRequest", exp);
			throw exp;
		}
		catch (Throwable exp)
		{
			logger.error("Unexcepted Exception grantBrokerAccessRequest", exp);
			out = false;
		}
		return out;
	}

	@PUT
	@Path("/accessrequests/reject")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})	
	public boolean rejectBrokerAccessRequest(
			@WebParam(name = "requests") List<BrokerAccessRequest> requests)
			throws NotAuthorizedException, AuthenicationRequiredException,
			NoSuchItemException, InvalidDataException, IncompleteDataException,
			FeatureNotAvailableException
	{
		boolean out = false;
		try
		{
			logger.debug("Calling rejectBrokerAccessRequest");
			out = brokerManagementImpl.rejectBrokerAccessRequest(getUser(),requests);
		}
		catch (PubSubException exp)
		{
			logger.warn("Expected possible Exception rejectBrokerAccessRequest", exp);
			throw exp;
		}
		catch (Throwable exp)
		{
			logger.error("Unexcepted Exception rejectBrokerAccessRequest", exp);
			out = false;
		}
		return out;

	}

}
