/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.hl7.services.eps.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.socraticgrid.hl7.services.eps.exceptions.AuthenicationRequiredException;
import org.socraticgrid.hl7.services.eps.exceptions.ConflictException;
import org.socraticgrid.hl7.services.eps.exceptions.ExpiredException;
import org.socraticgrid.hl7.services.eps.exceptions.FeatureNotAvailableException;
import org.socraticgrid.hl7.services.eps.exceptions.IncompleteDataException;
import org.socraticgrid.hl7.services.eps.exceptions.InvalidDataException;
import org.socraticgrid.hl7.services.eps.exceptions.MediaFormatNotExceptedException;
import org.socraticgrid.hl7.services.eps.exceptions.NoSuchItemException;
import org.socraticgrid.hl7.services.eps.exceptions.NoSuchTopicException;
import org.socraticgrid.hl7.services.eps.exceptions.NotAuthorizedException;
import org.socraticgrid.hl7.services.eps.interfaces.BrokerManagementIFace;
import org.socraticgrid.hl7.services.eps.model.AccessModel;
import org.socraticgrid.hl7.services.eps.model.BrokerAccessRequest;
import org.socraticgrid.hl7.services.eps.model.CreationResult;
import org.socraticgrid.hl7.services.eps.model.Durability;
import org.socraticgrid.hl7.services.eps.model.Options;
import org.socraticgrid.hl7.services.eps.model.PublicationContract;
import org.socraticgrid.hl7.services.eps.model.RemoveContentScope;
import org.socraticgrid.hl7.services.eps.model.Subscription;
import org.socraticgrid.hl7.services.eps.model.Topic;
import org.socraticgrid.hl7.services.eps.model.TopicMetadata;
import org.socraticgrid.hl7.services.eps.model.User;
import org.socraticgrid.hl7.services.eps.model.Role;
import org.socraticgrid.hl7.services.eps.internal.interfaces.SubscriptionIdGeneratorIFace;
import org.socraticgrid.hl7.services.eps.internal.interfaces.TopicIFace;
import org.socraticgrid.hl7.services.eps.internal.interfaces.TopicLocatorIFace;
import org.socraticgrid.hl7.services.eps.internal.model.KafkaBrokerBean;
import org.socraticgrid.hl7.services.eps.internal.model.TransientTopic;
import org.socraticgrid.hl7.services.eps.internal.processes.SubscriptionValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
 

/**
 * @author Jerry Goodnough
 *
 */
public class BrokerManagementServiceImpl {
	public BrokerManagementServiceImpl()
	{
		
	}
	
	//TODO:  Why is a user being injected here
	
	@Autowired(required = false)
	@Qualifier("Admin")
	private User adminUser;
	
	@Autowired
	TopicLocatorIFace topicLocator;
	
	@Autowired(required = false)
	KafkaBrokerBean kafkaBrokerBean;
	
	@Autowired
	SubscriptionIdGeneratorIFace idGen;

	@Autowired
	protected SubscriptionValidation subscriptionValidator;
	
	public CreationResult createTopic(User user, String parentTopic, String topicName,
			Topic topicOptions) throws NotAuthorizedException,
			AuthenicationRequiredException, ConflictException,
			NoSuchTopicException, MediaFormatNotExceptedException,
			ExpiredException, FeatureNotAvailableException,
			InvalidDataException, IncompleteDataException {
		
			CreationResult out = CreationResult.Failure;
		
			//TODO: Check general access privilege of the user
			
			//Locate the Parent Topic
			TopicIFace theParentTopic = topicLocator.locateTopic(parentTopic);
			if(theParentTopic == null) {
				NoSuchTopicException exp = new NoSuchTopicException();
				exp.setTopicId(parentTopic);
				throw exp;
			}
			
			//TODO:  Check if the parent or user has expired
			//TODO:  Determine if the user is allowed to add subtopics to the parent
			
			
			// Check is there is a conflict for the topic name
			if (theParentTopic.getSubTopic(topicName)!= null) 
			{
				ConflictException exp = new ConflictException();
				exp.setMessage("Subtopic is already defined in this parent topic");
				throw exp;
			}
			
			if (topicName.contains("/"))
			{
				throw new InvalidDataException("Bad topic name: "+topicName); 
			}
			
			//Force the Name
			topicOptions.setName(topicName);
			

			
			// Validate the Topic Options
			
			
			//Restrictions
			if (topicOptions.getSubtopics() != null && topicOptions.getSubtopics().isEmpty() == false)
			{
				throw new FeatureNotAvailableException("Cannot create topics with subtopics");
			}
			if (topicOptions.getAffiliationsList() != null && topicOptions.getAffiliationsList().isEmpty() == false)
			{
				throw new FeatureNotAvailableException("Cannot create topics with Afflications");
			}
			if (topicOptions.getPublicationReviewersList() != null && topicOptions.getPublicationReviewersList().isEmpty() == false)
			{
				throw new FeatureNotAvailableException("Cannot create topics with Publication Reviewers");
			}
			if (topicOptions.getDeliveryReviewersList() != null && topicOptions.getDeliveryReviewersList().isEmpty() == false)
			{
				throw new FeatureNotAvailableException("Cannot create topics with Delivery Reviewers");
			}
			if (topicOptions.getSubscriptionsList() != null && topicOptions.getSubscriptionsList().isEmpty() == false)
			{
				throw new FeatureNotAvailableException("Cannot create topics with Subscribers");
			}

			//Set up the topic if possible
			Options opts = topicOptions.getOptionsList();
			if (opts == null)
			{
				//FUTURE: Allow this to be configured or inherit from the parent
				opts = new Options();
				opts.setDurability(Durability.Transient);
				opts.setAccess(AccessModel.Open);
			}
			if (opts.getDurability() == Durability.Robust)
			{
				throw new FeatureNotAvailableException("Robust topics are not yet supported by dynamic creation");
			}
			
			//TODO: Convert the rest of this rest of this method to a task - We have in effect valfiated the creation be now.
			//If this changes to a task modekl then we could defer it as required.
			
			//Set expected Defaults
			if (opts.getDurability() == null)
			{
				opts.setDurability(Durability.Transient);
			}
			if (opts.getAccess() == null)
			{
				opts.setAccess(AccessModel.Open);
			}
			
			//Create the actual topic based on durability
			if (opts.getDurability() == Durability.Transient)
			{
				Topic topic= new Topic();
				topic.setDescription(topicOptions.getDescription());
				topic.setName(topicOptions.getName());
				topic.setParent(theParentTopic.getTopic());
				topic.setOptionsList(opts);
				//Transient Topic support
				TransientTopic transTopic = new TransientTopic();
				transTopic.setParentTopic(theParentTopic);
				transTopic.setTopic(topic);
				//FUTURE: Find a better way to do this - Maybe get from the parent
				transTopic.setIdGen(idGen);
				transTopic.setSubscriptionValidator(subscriptionValidator);
				//Add the topic to the parent
				theParentTopic.getSubTopics().put(topicName,transTopic);
				out = CreationResult.Success;
			}
			else
			{
				//Robust
				
			}
	  
			return out;
		
	}

	public boolean deleteTopic(User user, String topic) throws NotAuthorizedException,
			AuthenicationRequiredException, ConflictException,
			NoSuchTopicException {
		throw new NoSuchTopicException();
	}

	public boolean configureTopic(User user,String topic, Topic topicOptions)
			throws NotAuthorizedException, AuthenicationRequiredException,
			ConflictException, NoSuchTopicException,
			MediaFormatNotExceptedException, ExpiredException,
			FeatureNotAvailableException, InvalidDataException,
			IncompleteDataException {
		throw new FeatureNotAvailableException();
	}

	public TopicMetadata getTopicMetadata(User user, String topic)
			throws NotAuthorizedException, AuthenicationRequiredException,
			ExpiredException, NoSuchTopicException {
		throw new NoSuchTopicException();
	}

	public List<PublicationContract> getPublishersForTopic(User user, String topic)
			throws NotAuthorizedException, AuthenicationRequiredException,
			ExpiredException, NoSuchTopicException {
		
			List<Role> userRole = user.getPrivileges();
		   
			boolean pubAuth = false;
		  		 
			for (int i = 0; i < userRole.size();i++) 
			{
				if ( userRole.get(i) == Role.Admin || userRole.get(i) == Role.Normal ) 
				{
					pubAuth = true;
				}
			}
	
			if (pubAuth == false) 
			{
				throw new NotAuthorizedException();
			}
			else 
			{
	  		
				List<PublicationContract> outContract = new ArrayList<PublicationContract>();
    
				PublicationContract pubContract = new PublicationContract();    
				TopicIFace topicPub = topicLocator.locateTopic(topic);
	  		
				if (topicPub == null) 
				{
					throw new NoSuchTopicException();
				}
				else 
				{
					pubContract.setPublisher(user);
					outContract.add(pubContract);
				}
    
				return outContract;
			}	
	}

	public List<Subscription> getSubscriptionsForTopic(User user, String topic)
			throws NotAuthorizedException, AuthenicationRequiredException,
			ExpiredException, NoSuchTopicException {
		
			List<Role> userRole = user.getPrivileges();
		   
			boolean subAuth = false;
  		  		 
			for (int i = 0; i < userRole.size();i++) 
			{
				if ( userRole.get(i) == Role.Admin || userRole.get(i) == Role.Normal ) 
				{
					subAuth = true;
				}
			}
  		  
			if (subAuth == false) 
			{
				throw new NotAuthorizedException();
			}
			else 
			{
				TopicIFace topicSub = topicLocator.locateTopic(topic);
				   
				if (topicSub.getSubscriptions().isEmpty()) 
				{
					throw new NoSuchTopicException();
				}
				else 
				return topicSub.getSubscriptions();
			}
	}

	public boolean purgeAllTopicEvents(User user, String topic)
			throws NotAuthorizedException, AuthenicationRequiredException,
			NoSuchTopicException {
		throw new NoSuchTopicException();
	}

	public boolean createUser(User user, User userInfo) throws NotAuthorizedException,
			AuthenicationRequiredException, ConflictException,
			InvalidDataException, IncompleteDataException {
		return false;
	}

	public boolean deleteUser(User user, String userId, RemoveContentScope scope)
			throws NotAuthorizedException, AuthenicationRequiredException,
			NoSuchItemException {
		throw new NoSuchItemException();
	}

	public User getUser(User user, String userId) throws NotAuthorizedException,
			AuthenicationRequiredException, InvalidDataException,
			NoSuchItemException {
		throw new NoSuchItemException();
	}

	public List<User> discoverUser(User user, String query) throws NotAuthorizedException,
			AuthenicationRequiredException, InvalidDataException,
			NoSuchItemException {
		List<User> out = new LinkedList<User>();
		return out;
	}

	public void moveTopic(User user, String sourceTopic, String destinationPath)
			throws NotAuthorizedException, AuthenicationRequiredException,
			ConflictException, NoSuchTopicException {
		throw new NoSuchTopicException();
		
	}

	public void updateUser(User user, String userId, Options options, User userData)
			throws NotAuthorizedException, AuthenicationRequiredException,
			NoSuchItemException, MediaFormatNotExceptedException,
			InvalidDataException, IncompleteDataException {
		throw new NoSuchItemException();
		
	}

	public List<BrokerAccessRequest> getBrokerAccessRequests(User user)
			throws NotAuthorizedException, AuthenicationRequiredException,FeatureNotAvailableException {
		List<BrokerAccessRequest> out = new LinkedList<BrokerAccessRequest>();
		throw new FeatureNotAvailableException();
	}

	public boolean grantBrokerAccessRequest(User user,List<BrokerAccessRequest> request)
			throws NotAuthorizedException, AuthenicationRequiredException,
			NoSuchItemException, InvalidDataException, IncompleteDataException, FeatureNotAvailableException {
		throw new FeatureNotAvailableException();
	}

	public boolean rejectBrokerAccessRequest(User user,List<BrokerAccessRequest> requests)
			throws NotAuthorizedException, AuthenicationRequiredException,
			NoSuchItemException, InvalidDataException, IncompleteDataException,FeatureNotAvailableException {
	
		throw new FeatureNotAvailableException();
		
	}
	

	public CreationResult OldcreateTopic(User user, String parentTopic, String topicName,
			Topic topicOptions) throws NotAuthorizedException,
			AuthenicationRequiredException, ConflictException,
			NoSuchTopicException, MediaFormatNotExceptedException,
			ExpiredException, FeatureNotAvailableException,
			InvalidDataException, IncompleteDataException {
		
			CreationResult out;
			List<Role> userRole = adminUser.getPrivileges();
	   
			boolean userAuth = false;
	  		 
			for (int i = 0; i < userRole.size();i++) 
			{
				if ( userRole.get(i) == Role.Admin || userRole.get(i) == Role.Normal ) 
				{
					userAuth = true;
				}
			}
			
			if (userAuth == false) 
			{
				throw new NotAuthorizedException();
			}
			else 
			{
				
				//TODO: Figure out why on earth the topic is not retained
				
				final Producer<String,String> producer;
				Properties producerProps = new Properties();
				producerProps.put("metadata.broker.list", kafkaBrokerBean.getMetadataBrokerList());
				producerProps.put("serializer.class", kafkaBrokerBean.getSerializerClass());
				//producerProps.put("partitioner.class", KafkaProperties.partitionerClass);
				//producerProps.put("request.required.acks", KafkaProperties.requestRequiredAcks);
				ProducerConfig config = new ProducerConfig(producerProps);
	  		  
				producer = new Producer<String,String>(config);
	  		  
				KeyedMessage<String, String> data = new KeyedMessage<String,String>(parentTopic, topicName); 
	 		  
				producer.send(data);
	  
				producer.close();
	  
				out = CreationResult.Success;
			}
	  
			return out;
		//throw new FeatureNotAvailableException();
	}
	
}
