package org.socraticgrid.hl7.services.eps;

import java.security.Principal;

import javax.ws.rs.core.Context;

import org.apache.cxf.jaxrs.ext.MessageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.socraticgrid.hl7.services.eps.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

//import javax.servlet.http.HttpServletRequest;
//import org.apache.cxf.transport.http.AbstractHTTPDestination;

public class ServiceBase {
	// Static base anonymous user
	protected static User anonymousUser;

	static {
		anonymousUser = new User();
		anonymousUser.setName("Anonymous");
		anonymousUser.setUserId("0");
	}

	// @Context WebServiceContext jaxwsContext;
	@Context
	protected MessageContext jaxrsContext;

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired(required = false)
	@Qualifier("Anonymous")
	private User overrideAnonymousUser = null;

	// Get the Anonymous user - We allow Spring to overide this.
	protected User getAnonymousUser() {
		return overrideAnonymousUser != null ? overrideAnonymousUser : anonymousUser;
	}

	protected User getUser() {
		User usr = null;

		// The jaxrsContext should be a CXF composite support soap anfd rest
		// See:
		// http://cxf.apache.org/docs/jax-rs-and-jax-ws.html#JAX-RSandJAX-WS-Dealingwithcontexts

		Principal principle = null;

		if (jaxrsContext != null) {
			if (jaxrsContext.getSecurityContext() != null) {
				principle = jaxrsContext.getSecurityContext().getUserPrincipal();
			} else {
				String ipAddress="unknown";
				logger.debug("No security context availabe");
				/* TODO - Resolve HttpServletRequest and Test
				 * 
				    <dependency>
						<groupId>javax.servlet</groupId>
						<artifactId>servlet-api</artifactId>
						<version>[2.5,)</version>
						<scope>provided</scope>
	  				</dependency>
  					
  					
				HttpServletRequest request = (HttpServletRequest) jaxrsContext
						.get(AbstractHTTPDestination.HTTP_REQUEST);
				if (request != null) {
					if (StringUtils.isNotBlank(request.getHeader("X-Forwarded-For"))) {
						ipAddress = request.getHeader("X-Forwarded-For");
					} else {
						ipAddress = request.getRemoteAddr();
					}
				}
				logger.debug("Detected client ip as "+ipAddress);
				*/
			}
		} else {
			logger.debug("jaxrsContext is null");
		}

		if (principle != null) {
			usr = this.getUserFromPrinciple(principle);
		} else {
			// Anonymous Access
			usr = anonymousUser;
		}
		logger.debug("User = " + usr);
		return usr;
	}

	protected User getUserFromPrinciple(Principal webUser) {
		// FUTURE: Lookup the user andf implement the open model

		User user = new User();
		user.setName(webUser.getName());
		// TODO: Update ID model to reflect ID lookup
		user.setUserId(webUser.getName());
		return user;
	}
}
